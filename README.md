
# Address Book server

Server du projet de gestion d'un carnet d'adresse

# Application serveur pour la gestion d'un carnet d'adresse

Ici on a le squelette de base de l'application backend fait avec NodeJs et Types

## Mise en place
 executer :
* npm i //pour initialer le projet et mettre à jour les dependences

Créer le fichier .env en prennant exemple sur le fichier .env.example et placez y vos configuration local

executer les commandes:

* npm run dev // demarrer en mode dev (le serveur redemarre à chaque modification de fichier dans le repertoire src)
* npm run start // demarrer en mode production
* npm run prod // demarrer en mode production avec pm2

acceder à l'adresse localhost:{.env.PORT}/
et vous recevrer la reponse suivant:

## Lien util

* https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-ubuntu-18-04

* https://github.com/Unitech/pm2/issues/3924 (erreur lors du demarage du service pm2)

## Ressources disponibles

* http://localhost:5000/api/v1/group/ (Gestion des groupes)

* http://localhost:5000/api/v1/address/ (Gestion des adresses)