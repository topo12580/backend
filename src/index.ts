import * as express from 'express';
import {londoContainer} from "./app/config/InversifyConfig";
import {InversifyExpressServer} from "inversify-express-utils";
import path from "path";
import {BaseMiddleWare} from "./app/middleware/BaseMiddleWare";
import * as dotenv from 'dotenv';

import "./app/controller/GroupController"
import "./app/controller/AddressController"

let container = londoContainer;

let serer = new InversifyExpressServer(container);

serer.setConfig((app) => {
	
	app.all('*', function(req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', '*');
        res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');

        if (req.method == 'OPTIONS') {
            res.send(200);
        } else {
            next();
        }
    });
    app.use(BaseMiddleWare.configuration());
    app.use(express.static(path.join(__dirname + "../public")));
    app.use('/*.js', (req: express.Request, res: express.Response) => {
        res.sendFile(process.cwd() + '/public/client' +  req.baseUrl);
    })
    app.use('/*.png', (req: express.Request, res: express.Response) => {
        res.sendFile(process.cwd() + '/public/client' +  req.baseUrl);
    });

});

dotenv.config();

let app = serer.build();

console.log(dotenv);

exports = module.exports = app;