#!/usr/bin/env node

import dotenv from "dotenv";
import express from "express";
import {MongoConfig} from "../app/config/MongoConfig";

/**
 * Module dependencies.
 */

const app = require('../index');
const debug = require('debug')('house-leave:server');
const http = require('http');

dotenv.config({path: process.cwd() + '/src/config.env'});

app.get("/", (req: express.Request, res: express.Response) => {
  res.sendFile(process.cwd() + '/public/client/index.html');
});

/**
 * Get port and host from environment and store in Express.
 */

const port = normalizePort(process.env.PORT || '3000');
const host = process.env.HOST || 'localhost';
app.set('port', port);
app.set('host', host);

/**
 * Create HTTP server.
 */

const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port, host);
server.on('error', onError);
server.on('listening', onListening);
server.on('listening', mongooseConnection);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val: any) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error: any) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

function mongooseConnection() {
  MongoConfig.connect()
      .catch((err)=> {throw new Error(err)})
}