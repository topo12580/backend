import express from 'express';
import bodyParser from 'body-parser';
import path from "path";
/**
 * Inject objects we need to parse a request and configure a response
 *
 * @author Yvan KOMBOU
 * @since 01/07/2020
 * @version 0.0.1
 */
export class BaseMiddleWare {
    static configuration(): express.Application {
        const app: express.Application = express();
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({extended: true}));
        app.use(express.static(path.join(__dirname + '../../public')));
        return app;
    }
}