export class Constants {
    constructor() {
    }
    static DB_OPTIONS: any = {
        socketTimeoutMS: 30000,
        keepAlive: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        reconnectTries: 30000
    }
}