import { Container } from "inversify";

import "reflect-metadata";
import { AddressModel, IAddress } from "../model/Address";
import { GroupModel, IGroup } from "../model/Group";
import { AddressRepository } from "../repository/AddressRepository";
import { GroupRepository } from "../repository/GroupRepository";
import { AddressService } from "../service/AddressRepository";
import { GroupService } from "../service/GroupService";
import { TYPES } from "./constant/types";
let container = new Container();

container.bind<IGroup>(TYPES.IGroup).to(GroupModel);
container.bind<IAddress>(TYPES.IAddress).to(AddressModel);

container.bind<GroupRepository>(TYPES.GroupRepository).to(GroupRepository);
container.bind<AddressRepository>(TYPES.AddressRepository).to(AddressRepository);

container.bind<GroupService>(TYPES.GroupService).to(GroupService);
container.bind<AddressService>(TYPES.AddressService).to(AddressService);


export const londoContainer = container;