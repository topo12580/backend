import mongoose from "mongoose";
import {Constants} from "../utils/Constants";

/**
 *
 */
export class MongoConfig {
    static url: string = process.env.DATABASE_URL || 'mongodb://161.97.183.189:27017:27017/address-book';

    static _mongooseInstance: any = null;

    static connect(): Promise<mongoose.Connection> {
        if (this._mongooseInstance !== null) return this._mongooseInstance;
        return new Promise<mongoose.Connection>((resolve) => {
            let interval = setInterval(()=> {
                this._mongooseInstance = mongoose.connect(this.url, Constants.DB_OPTIONS, (err: any) => {
                    if (err){
                        console.log('App cannot connect to database we will try again after 3 seconds 😒');
                    } else {
                        clearInterval(interval);
                        resolve(this._mongooseInstance);
                        console.log('App connected to database !!! 😊');
                    }
                })
            }, 3000)
        });
    }

}