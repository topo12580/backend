export const TYPES = {
    IGroup: Symbol.for("IGroup"),
    IAddress: Symbol.for("IAddress"),
    GroupRepository: Symbol.for("GroupRepository"),
    AddressRepository: Symbol.for("AddressRepository"),
    GroupService: Symbol.for("GroupService"),
    AddressService: Symbol.for("AddressService"),
    NAME_OF_DEFAULT_GROUP: "Adresses Personnelles",
  };