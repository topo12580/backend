import { injectable } from "inversify";
import { AddressModel, IAddressModel } from "../model/Address";
import { BaseRepository } from "./base/BaseRepository";

@injectable()
export class AddressRepository extends BaseRepository<IAddressModel>{
    constructor() {
        super(AddressModel);
    }
}