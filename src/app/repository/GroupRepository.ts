import { injectable } from "inversify";
import { GroupModel, IGroupModel } from "../model/Group";
import { BaseRepository } from "./base/BaseRepository";

@injectable()
export class GroupRepository extends BaseRepository<IGroupModel>{
    constructor() {
        super(GroupModel);
    }

    findGroupByName(name: string): Promise<IGroupModel> {
        return this._model.findOne({name});
    }
}