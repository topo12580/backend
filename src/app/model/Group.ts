import { Model, Schema, SchemaTypes, Document, model } from "mongoose";
import { IAddress } from "./Address";

export interface IGroup {
    name: string;
    itemCount: number;
    addresses: Array<IAddress>;
    parent: IGroup;
}

export interface IGroupModel extends IGroup, Document {
    
}

export var GroupSchema: Schema = new Schema({
    name: String,
    addresses: [
        { type: SchemaTypes.ObjectId, ref: "Address" }
    ],
    parent: { type: SchemaTypes.ObjectId, ref: "Group" },
    itemCount: Number,
});

export const GroupModel: Model<IGroupModel> = model<IGroupModel>("Group", GroupSchema);