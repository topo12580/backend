import { Schema, Document, SchemaTypes, Model, model } from "mongoose";

export interface IAddress {
    name: string;
    surname: string;
    email: string;
    phoneContact: number
    pic: string,
    group: any;
}

export interface IAddressModel extends IAddress, Document {

}

export var AddressSchema: Schema = new Schema({
    name: String,
    surname: String,
    email: String,
    phoneContact: Number,
    group: { type: SchemaTypes.ObjectId, ref: "Group" },
    pic: String,
});

export const AddressModel: Model<IAddressModel> = model<IAddressModel>("Address", AddressSchema);