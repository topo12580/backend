import { inject, injectable } from "inversify";
import { TYPES } from "../config/constant/types";
import { GroupModel, IGroupModel } from "../model/Group";
import { GroupRepository } from "../repository/GroupRepository";

@injectable()
export class GroupService {
    private groupReposetory: GroupRepository;

    public constructor(@inject(TYPES.GroupRepository) repository: GroupRepository) {
        this.groupReposetory = repository;
    }

    createDefaultGroup(): Promise<IGroupModel> {
        const defaultGroup: IGroupModel = new GroupModel();
        defaultGroup.name = TYPES.NAME_OF_DEFAULT_GROUP;
        return this.groupReposetory.create(defaultGroup);
    }

    findDefaultGroup(): Promise<IGroupModel> {
        return this.groupReposetory.findGroupByName(TYPES.NAME_OF_DEFAULT_GROUP)
    }
}