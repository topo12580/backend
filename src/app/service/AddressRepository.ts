import { inject, injectable } from "inversify";
import { TYPES } from "../config/constant/types";
import { AddressRepository } from "../repository/AddressRepository";

@injectable()
export class AddressService {
    private addressReposetory: AddressRepository;

    public constructor(@inject(TYPES.AddressRepository) repository: AddressRepository) {
        this.addressReposetory = repository;
    }
}