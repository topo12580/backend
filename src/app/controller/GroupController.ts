import express from "express";
import { inject } from "inversify";
import { controller, httpDelete, httpGet, httpPost, httpPut, request, response} from "inversify-express-utils";
import { TYPES } from "../config/constant/types";
import { IGroupModel } from "../model/Group";
import { GroupRepository } from "../repository/GroupRepository";
import { GroupService } from "../service/GroupService";

/**
 * Controller for group.
 *
 * @author Yvan KOMBOU
 * @since 11/05/2021
 * @version 0.0.1
 */
 @controller('/api/v1/group')
export class GroupController {
    private groupRepository: GroupRepository;
    private groupService: GroupService;

    constructor(
        @inject(TYPES.GroupService) service: GroupService,
        @inject(TYPES.GroupRepository) repository: GroupRepository) {
        this.groupRepository = repository;
        this.groupService = service;
    }

    @httpGet("/")
    public async get(@request() req: express.Request, @response() res: express.Response) {
        console.log("Get request Group controller");
        try {
            const countryList = await this.groupRepository.retrieve();
            res.json(countryList);
        } catch (error) {
            return res.status(500).send(error.message);
        };
    }

    @httpGet("/:id")
    public async getById(@request() req: express.Request, @response() res: express.Response) {
        console.log("Get request of record controller");
        try {
            const group = await this.groupRepository.findById(req.params.id);
            res.json(group);
        } catch (error) {
            res.status(500).send(error.message);
        }
    }

    @httpPost("/")
    public async post(@request() req: express.Request, @response() res: express.Response) {
        console.log("Post request of record controller");
        try {
            const group = req.body as IGroupModel;
            delete group._id;

            const ob = await this.groupRepository.findGroupByName(group.name);
            if (ob) {
                res.status(500).send('group.create.alrady.exist');
                res.end();
            } else {
                if (!group.parent) {
                    let defaultGroup = await this.groupService.findDefaultGroup();
                    
                    if (!defaultGroup) {
                        defaultGroup = await this.groupService.createDefaultGroup();
                    }
    
                    group.parent = defaultGroup;
                }
    
                const newGroup = await this.groupRepository.create(group);
                res.json(newGroup);
            }
        } catch (error) {
            res.status(500).send(error.message);
        }
    }

    @httpPut("/:id")
    public async put(@request() req: express.Request, @response() res: express.Response) {
        console.log("Put request of country controller");
        try {
            const group = await this.groupRepository.update(req.params.id, req.body);
            res.json(group);
        } catch (error) {
            res.status(500).send(error.message);
        }
    }

    @httpDelete("/:id")
    public async delete(@request() req: express.Request, @response() res: express.Response) {
        console.log("Delete request of country controller");
        try {
            const group = await this.groupRepository.delete(req.params.id);
            res.json(group);
        } catch (error) {
            res.status(500).send(error.message);
        }
    }
}