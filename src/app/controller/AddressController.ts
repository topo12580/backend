import express from "express";
import { inject } from "inversify";
import { controller, httpDelete, httpGet, httpPost, httpPut, request, response} from "inversify-express-utils";
import { TYPES } from "../config/constant/types";
import { IAddress, IAddressModel } from "../model/Address";
import { AddressRepository } from "../repository/AddressRepository";
import { GroupRepository } from "../repository/GroupRepository";
import { GroupService } from "../service/GroupService";

/**
 * Controller for address.
 *
 * @author Yvan KOMBOU
 * @since 11/05/2021
 * @version 0.0.1
 */
 @controller('/api/v1/address')
export class AddressController {

    private groupRepository: GroupRepository;
    private addressRepository: AddressRepository;
    private groupService: GroupService;

    constructor(
        @inject(TYPES.GroupService) groupService: GroupService,
        @inject(TYPES.AddressRepository) repository: AddressRepository,
        @inject(TYPES.GroupRepository) grouprepository: GroupRepository) {
        this.addressRepository = repository;
        this.groupRepository = grouprepository;
        this.groupService = groupService;
    }

    @httpGet("/")
    public async get(@request() req: express.Request, @response() res: express.Response) {
        console.log("Get request of Address controller");
        try {
           
            let output = await new Promise(async (resolve, reject) => {
                this.addressRepository.retrieve().then((data: any) => {
                    resolve(data);
                }).catch(error => {
                    if (error) {
                        reject(error);
                    }
                });
            });
            return res.json(output);
        } catch (error) {
            return res.status(500).send(error.message);
        };
    }

    @httpGet("/:id")
    public async getById(@request() req: express.Request, @response() res: express.Response) {
        console.log("Get request of Address controller");
        try {
            const address = await this.addressRepository.findById(req.params.id);
            res.json(address);
        } catch (error) {
            res.status(500).send(error.message);
        }
    }

    @httpPost("/")
    public async post(@request() req: express.Request, @response() res: express.Response) {
        console.log("Post request of address controller");
        try {
            const address = req.body as IAddressModel;
            delete address._id;
            let group = await this.groupRepository.findGroupByName(address.group.name as string)

            if(!group){
                group = await this.groupService.findDefaultGroup();
            }
            address.group  = group;
            const newAddress= await this.addressRepository.create(address);
            group.addresses.push(newAddress._id);
            const result = await this.groupRepository.update(group._id, group);
            res.json(newAddress);
        } catch (error) {
            res.status(500).send(error.message);
        }
    }

    @httpPut("/:id")
    public async put(@request() req: express.Request, @response() res: express.Response) {
        console.log("Put request of address controller");
        try {
            const address = await this.addressRepository.update(req.params.id, req.body);
            res.json(address);
        } catch (error) {
            res.status(500).send(error.message);
        }
    }

    @httpDelete("/:id")
    public async delete(@request() req: express.Request, @response() res: express.Response) {
        console.log("Delete request of address controller");
        try {
            const address = await this.addressRepository.findById(req.params.id);
            const group = await this.groupRepository.findGroupByName(address.group.name);
            let index = group.addresses.indexOf((address as IAddress));
            if(index>-1){
                group.addresses.splice(index, 1);
                await this.groupRepository.update(group._id, group);
            }
            res.json(address);
        } catch (error) {
            res.status(500).send(error.message);
        }
    }


}